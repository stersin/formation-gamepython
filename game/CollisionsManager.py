class CollisionsManager(object):

    def __init__(self):
        self._entities = []

    def appendCollidable(self, collidable):
        self._entities.append(collidable)

    def checkCollisions(self):
        toCheck = self._entities[::]

        while len(toCheck):
            entity = toCheck.pop()
            entity.resolveCollisions(toCheck)
