import pygame

from game.Entity import Entity
from game.Collidable import Collidable
from game.CollisionsManager import CollisionsManager


class Container(Entity):

    def __init__(self, game, container, entities=[]):
        super(Container, self).__init__(game, container)
        self._entities = entities[::]

    @property
    def entities(self):
        return self._entities

    # Entity methods

    def draw(self, screen, camera):
        for entity in self._entities:
            entity.draw(screen, camera)

    def handleEvent(self, event):
        for entity in self._entities:
            entity.handleEvent(event)

    def update(self, time):
        for entity in self._entities:
            entity.update(time)

    def appendEntity(self, entity):
        self._entities.append(entity)
        entity.container = self

        if isinstance(entity, Collidable):
            self._game.collisionsManager.appendCollidable(entity)
