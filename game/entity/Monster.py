import os, sys

import pygame
from pygame.locals import *

from game.Entity import Entity
from lib.Vec2d import Vec2d
from game.Collidable import Collidable
from game.entity import Bullet

class Monster(Entity, Collidable):
    _conf = {
        'idle': 
        {
            'horizontalMaxSpeed': 0,
            'horizontalForce': 0,
            'horizontalFriction': 4000,
            'verticalMaxSpeed': 0,
            'verticalForce': 0
        },
        'walking': 
        {
            'horizontalMaxSpeed': 400,
            'horizontalForce': 1000,
            'horizontalFriction': 4000,
            'verticalMaxSpeed': 0,
            'verticalForce': 0
        },
        'running': 
        {
            'horizontalMaxSpeed': 800,
            'horizontalForce': 1500,
            'horizontalFriction': 4000,
            'verticalMaxSpeed': 0,
            'verticalForce': 0
        },
        'falling': 
        {
            'horizontalMaxSpeed': 800,
            'horizontalForce': 500,
            'horizontalFriction': 1000,
            'verticalMaxSpeed': 500,
            'verticalForce': 1400
        },
        'jumping':
        {
            'horizontalMaxSpeed': 0,
            'horizontalForce': 0,
            'horizontalFriction': 0,
            'verticalMaxSpeed': 0,
            'verticalForce': 17000
        }
    }
    
    def __init__(self, game, container = None):
        super(Monster, self).__init__(game, container)
    
        self._state = 'idle'
        self._speedKeyDown = False
        self._onGround = False
        self._input = Vec2d(0, 0)
        self._speed = Vec2d(0, 0)
        self._lookAt = Vec2d(1, 0)
        self._life = 100
                
        self._mainSprite = pygame.sprite.Sprite()
        self._mainSprite.image, self._mainSprite._rect = self.__loadImage('megaman.png', -1)
        
        self._mainSprite._rect = self._mainSprite._rect.fit(pygame.Rect(0, 0, 100, 100))
        self._mainSprite.image = pygame.transform.smoothscale(self._mainSprite.image, (self._mainSprite._rect.width, self._mainSprite._rect.height))
        
        self._collideRect = self._mainSprite._rect.copy()
    
    def __loadImage(self, name, colorkey=None):
        fullname = os.path.join('resources', name)
        try:
            image = pygame.image.load(fullname)
        except pygame.error, message:
            print 'Cannot load image:', name
            raise SystemExit, message
        image = image.convert()
        if colorkey is not None:
            if colorkey is -1:
                colorkey = image.get_at((0, 0))
            image.set_colorkey(colorkey, RLEACCEL)
        return image, image.get_rect()
        
        
        
    # Entity methods
        
    def draw(self, screen, camera):
        self._mainSprite._rect.left = self._collideRect.left - camera.worldRect.left
        self._mainSprite._rect.top = self._collideRect.top - camera.worldRect.top
        
        surfaceToDraw = self._mainSprite.image
        
        if self._lookAt.x < 0:
            surfaceToDraw = pygame.transform.flip(surfaceToDraw, True, False)
        
        cameraSurface = camera.worldSurface
        
        if self._collideRect.colliderect(camera.worldRect):
            cameraSurface.blit(surfaceToDraw, self._mainSprite._rect)
        
        screenSize = cameraSurface.get_size()
        debugText = pygame.font.Font(None, 36).render('%s %s %f,%f %f,%f' % (self._state, self._onGround, self._input.x, self._input.y, self._collideRect.left, self._collideRect.bottom), 1, (0, 0, 0))
        
        height = debugText.get_height()
        
        cameraSurface.blit(debugText, (screenSize[0] - debugText.get_width() - 10, screenSize[1] - height - 10))
        
        debugText = pygame.font.Font(None, 36).render('%s %s' % (self._mainSprite._rect, self._collideRect), 1, (10, 10, 10))
        cameraSurface.blit(debugText, (screenSize[0] - debugText.get_width() - 10, screenSize[1] - debugText.get_height() - 10 - height - 10))
    
    def handleEvent(self, event):        
        return True
    
    def update(self, time):
        # handle state change
        if self._onGround:
            if abs(self._input.x) == 1:
                if self._speedKeyDown == True:
                    self._state = 'running'
                else:
                    self._state = 'walking'
            else:
                self._state = 'idle'
        elif self._state != 'jumping':
            self._state = 'falling'                

        # handle jumping input        
        if self._input.y < 0 and self._state != 'jumping' and self._state != 'falling':
            self._state = 'jumping'
            self._onGround = False
        
        if self._state == 'jumping':
            self._speed.y += self._input.y * self._conf[self._state]['verticalForce'] * time
             
            self._state = 'falling'
        
        # handle falling _state
        if self._state == 'falling':
            self._speed.y += self._conf[self._state]['verticalForce'] * time
            
            if self._speed.y > self._conf[self._state]['verticalMaxSpeed']:
                self._speed.y = self._conf[self._state]['verticalMaxSpeed']
                                        
        # handle horizontal _speed 
        if self._input.x != 0:            
            self._speed.x += self._input.x * self._conf[self._state]['horizontalForce'] * time
            if abs(self._speed.x) > self._conf[self._state]['horizontalMaxSpeed']:
                self._speed.x = self._input.x * self._conf[self._state]['horizontalMaxSpeed']
        elif self._speed.x > 0:
            self._speed.x -= self._conf[self._state]['horizontalFriction'] * time
            if self._speed.x < 0:
                self._speed.x = 0
        elif self._speed.x < 0:
            self._speed.x += self._conf[self._state]['horizontalFriction'] * time
            if self._speed.x > 0:
                self._speed.x = 0
            
        # update pos
        self._collideRect.left += self._speed.x * time
        self._collideRect.bottom += self._speed.y * time
        
        # need to trigger collision to continue 
        if self._onGround:
            self._collideRect.bottom += 1
            self._onGround = False
                    
    # Collidable methods
    def onCollide(self, entity):
        thisRect = self._collideRect
        otherRect = entity.collideRect
        
        # colliding from bottom to top    
        if thisRect.top > otherRect.bottom and thisRect.top < otherRect.top:
            self._speed.y = 0
            thisRect.top = otherRect.bottom
            
        # colliding from bottom to top    
        elif thisRect.bottom > otherRect.top and thisRect.bottom < otherRect.bottom:
            self._speed.y = 0
            thisRect.bottom = otherRect.top
            self._onGround = True
                    
        # colliding from right to left
        elif thisRect.left < otherRect.right and thisRect.left > otherRect.left:
            self._speed.x = 0
            thisRect.left = otherRect.right
        
        # colliding from left to right    
        elif thisRect.right > otherRect.left and thisRect.right < otherRect.right:
            self._speed.x = 0
            thisRect.right = otherRect.left
               
               
    def fire(self):
        if self._lookAt.x > 0:
            bulletPos = Vec2d(self._collideRect.right, self._collideRect.centery)
            bulletSpeed = Vec2d(600, 0)
        else:
            bulletPos = Vec2d(self._collideRect.left, self._collideRect.centery)
            bulletSpeed = Vec2d(-600, 0)
            
        bullet = Bullet.Bullet(self._game, None, self, bulletPos, bulletSpeed)
        self._game.scene.appendEntity(bullet)
