import pygame

from game.Entity import Entity
from game.Collidable import Collidable

class Bullet(Entity, Collidable):
    def __init__(self, game, container, owner, pos, speed):
        super(Bullet, self).__init__(game, container = None)
        
        self._owner = owner
        self._speed = speed
        self._collideRect = pygame.Rect(pos.x, pos.y, 10, 10)
        self._maxLifetime = 1
        self._lifetime = 0
        
        self._surface = pygame.Surface((self._collideRect.width, self._collideRect.height))
        pygame.draw.rect(self._surface, pygame.Color('red'), self._collideRect)
    
    # Entity methods
    
    def draw(self, screen, camera):
        surface = camera.worldSurface
                
        if self._collideRect.colliderect(camera.worldRect):
            surface = camera.worldSurface
            surface.blit(self._surface, (self._collideRect.x - camera.worldRect.x, self._collideRect.y - camera.worldRect.y))
            
        cameraSurface = camera.worldSurface
        debugText = pygame.font.Font(None, 36).render('%s, %f' % (self._collideRect, self._lifetime), 1, (0, 0, 0))
        cameraSurface.blit(debugText, (10, 10))    
            
    def handleEvent(self, event):
        pass
    
    def update(self, time):
        self._lifetime += time
        
        if self._lifetime > self._maxLifetime:
            self._game.scene.entities.remove(self)
        else:
            self._collideRect.centerx += self._speed.x * time 
            self._collideRect.centery += self._speed.y * time

    def onCollide(self, entity):
	pass
