import pygame
from pygame.locals import *
from game.entity.Monster import Monster

class Player(Monster):
    
    def handleEvent(self, event):        
        if event.type == KEYDOWN:
            shift = pygame.key.get_mods() & KMOD_SHIFT
            
            if event.key == K_RIGHT:
                self._input.x = 1
                self._lookAt.x = 1
            elif event.key == K_LEFT:
                self._input.x = -1
                self._lookAt.x = -1
            elif event.key == K_SPACE:
                self._input.y = -1
            elif event.key == K_x:
                self.fire()
                
            if shift:
                self._speedKeyDown = True
                
        elif event.type == KEYUP:
            self._speedKeyDown = False
             
            if event.key == K_RIGHT or event.key == K_LEFT:
                self._input.x = 0
            elif event.key == K_SPACE:
                self._input.y = 0
            
        return True