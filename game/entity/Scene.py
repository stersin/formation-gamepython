import pygame
from pygame.locals import *

from game.entity.Container import Container
from game.entity.Player import Player
from game.entity.Level1 import Level1

class Scene(Container):        
    @property
    def player(self):
        return self._player
    
    @property
    def level(self):
        return self._level
        
    def __init__(self, game, container):
        super(Scene, self).__init__(game, container)
        
        self._player = Player(game, self)
        self._level = Level1(game, self)
        
        self.appendEntity(self._player)
        self.appendEntity(self._level)