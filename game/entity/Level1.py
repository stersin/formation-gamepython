import pygame
from pygame.locals import *

from game.entity.Container import Container
from game.entity.LevelObject import LevelObject

class Level1(Container):
    @property
    def rect(self):
        return self._rect
    
    def __init__(self, game, container = None):
        super(Level1, self).__init__(game, container)
                        
        self.appendEntity(LevelObject(game, pygame.Rect(50, 300, 200, 50)))
        self.appendEntity(LevelObject(game, pygame.Rect(150, 220, 200, 50)))
        self.appendEntity(LevelObject(game, pygame.Rect(400, 300, 200, 50)))
        self.appendEntity(LevelObject(game, pygame.Rect(600, 300, 2000, 50)))
                
        self._rect = pygame.Rect(0,0, 2000, 1000)
    # Entity methods
                    
    def handleEvent(self, event):        
        if event.type == KEYDOWN:            
            if event.key == K_RETURN:
                self.restart()
                
    def restart(self):
        self._game.scene.player.collideRect.left = 70
        self._game.scene.player.collideRect.bottom = 250