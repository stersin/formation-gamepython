import pygame

from game.Entity import Entity

class Camera(Entity):
    
    def __init__(self, game, scene, rect, color):
        super(Camera, self).__init__(game)
        
        self._followEntity = None
        self._color = color      
        self._scene = scene
        self._renderRect = rect
        self._renderSurface = pygame.Surface((self._renderRect.width, self._renderRect.height))
        self._worldRect = pygame.Rect(100,50,800,600)  
        self._worldSurface = pygame.Surface((self._worldRect.width, self._worldRect.height))
    
    # Properties
        
    @property
    def worldRect(self):
        return self._worldRect
    
    @property
    def worldSurface(self):
        return self._worldSurface
       
    # Entity methods        
    def draw(self, screen, camera):        
        self._worldSurface.fill(self._color)                
        self._scene.draw(screen, self)        
        
        renderSize = self._worldSurface.get_size()
        debugText = pygame.font.Font(None, 36).render('%s' % (self._worldRect), 1, (10, 10, 10))
        self._worldSurface.blit(debugText, (renderSize[0] - debugText.get_width() - 10, 0 + 10))
        height = debugText.get_height()
        debugText = pygame.font.Font(None, 36).render('%s' % (self._renderRect), 1, (10, 10, 10))
        self._worldSurface.blit(debugText, (renderSize[0] - debugText.get_width() - 10, 0 + 10 + height + 10))
        
        pygame.transform.scale(self._worldSurface, (self._renderRect.width, self._renderRect.height), self._renderSurface)        
        screen.blit(self._renderSurface, (self._renderRect.left, self._renderRect.top))
    
    def handleEvent(self, event):
        pass
    
    def update(self, time):
        if self._followEntity:
            followRect = self._followEntity.collideRect
            self._worldRect.top = followRect.top - self._worldRect.height / 2 + followRect.height / 2
            self._worldRect.left = followRect.left - self._worldRect.width / 2 + followRect.width / 2
            
        
    # Self methods
    
    def followEntity(self, entity):
        self._followEntity = entity
    
    
    
    