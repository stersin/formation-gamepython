import pygame

from game.Entity import Entity
from game.Collidable import Collidable

class LevelObject(Entity, Collidable):
    def __init__(self, game, rect, container = None):
        super(LevelObject, self).__init__(game, container)
        self._collideRect = rect
        self._surface = pygame.Surface((self._collideRect.width, self._collideRect.height))
        pygame.draw.rect(self._surface, pygame.Color('red'), self._collideRect)
    
    # Entity methods
    
    def draw(self, screen, camera):
        surface = camera.worldSurface
#        rect = self._collideRect.move(, )
                
        if self._collideRect.colliderect(camera.worldRect):
            surface = camera.worldSurface
            surface.blit(self._surface, (self._collideRect.x - camera.worldRect.x, self._collideRect.y - camera.worldRect.y))
            
    def handleEvent(self, event):
        pass
    
    def update(self, time):
        pass
    