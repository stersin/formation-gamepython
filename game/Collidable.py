import pygame
from lib.Vec2d import Vec2d


class Collidable(object):

    @property
    def collideRect(self):
        return self._collideRect

    def resolveCollisions(self, entities):
        for entity in entities:
            if isinstance(entity, Collidable) and entity != self:
                if self.collideRect.colliderect(entity.collideRect):
                    self.onCollide(entity)

    def onCollide(self, entity):
        raise NotImplementedError
