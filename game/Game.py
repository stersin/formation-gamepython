import logging
import pygame
from pygame.locals import *
from game.entity.Camera import Camera
from game.entity.Scene import Scene
from game.CollisionsManager import CollisionsManager
from lib import GameClock

if not pygame.font: print 'Warning, fonts disabled'
if not pygame.mixer: print 'Warning, sound disabled'


class Game(object):
    """Main Game class"""

    @property
    def scene(self):
        return self._scene

    @property
    def collisionsManager(self):
        return self._collisionsManager

    def __init__(self):
        logging.basicConfig(filename='logs/main.log', filemode='w', level=logging.DEBUG)

        pygame.init()

        self._running = True

        # screen
        self._screen = pygame.display.set_mode((800, 600))
        pygame.display.set_caption('My first game')
        pygame.mouse.set_visible(0)

        self._collisionsManager = CollisionsManager()

        # create scene and cameras   
        self._scene = Scene(self, None)

        self._cameras = []
        self._cameras.append(Camera(self, self._scene, pygame.Rect(0, 0, self._screen.get_width(), self._screen.get_height()), (150, 250, 150)))
        self._cameras.append(Camera(self, self._scene,
                                    pygame.Rect(250, 250, self._screen.get_width() / 2, self._screen.get_height() / 2),
                                    (50, 100, 100)))
        self._cameras.append(
                Camera(self, self._scene,
                       pygame.Rect(25, 25, self._screen.get_width() / 3, self._screen.get_height() / 3),
                       (0, 150, 0)))

        self._cameras[0].followEntity(self._scene.player)

    def run(self):
        clock = GameClock.GameClock(
                update_callback=self.__updateCallback,
                frame_callback=self.__frameCallback,
                paused_callback=None)

        while self._running:
            clock.tick()

    def __frameCallback(self, time):
        self._screen.fill((255, 255, 255))

        for camera in self._cameras:
            camera.draw(self._screen, None)

        pygame.display.flip()

    def __updateCallback(self, time):
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                self._running = False
            else:
                for camera in self._cameras:
                    camera.handleEvent(event)

                self._scene.handleEvent(event)

        for camera in self._cameras:
            camera.update(time)

        self._scene.update(time)

        self._collisionsManager.checkCollisions()

    def __pauseCallback(self):
        pass
