class Entity(object):    
    def __init__(self, game, container = None):
        self._game = game
        self._container = container
       
    def destroy(self):
        if self._container:
            self._container.entities.remove(self)
            self.container = None
       
    def draw(self, screen):
        raise NotImplementedError
    
    def handleEvent(self, event):
        raise NotImplementedError
    
    def update(self, time):
        raise NotImplementedError
    
    @property
    def container(self):
        return self._container
    
    @container.setter
    def container(self, value):
        # Do something if you want
        self._container = value